# 02-dom-wasm

## Requirements

- Create a `go.mod` file
- Create a `main.go` file

## go.mod

```text
module 02-dom-wasm

go 1.17
```

## main.go

```golang
package main

import (
	"syscall/js"
)

func main() {

	message := "👋 Hello World 🌍 from Go"

	document := js.Global().Get("document")

	h2 := document.Call("createElement", "h2")
	h2.Set("innerHTML", message)
	
	document.Get("body").Call("appendChild", h2)

}
```

## Build, run, ...

```bash
# build the wasm fil
GOOS=js GOARCH=wasm go build -o main.wasm
```

```bash
# run the http server
node index.js
```

- Open the webpage in your browser

## Change the color of the h1 and h2 tags

- **orange** for `h2`
  ```javascript
  document.querySelector("h2").style.color="orange"
  ```
- **green** for `h1`
  ```javascript
  document.querySelector("h1").style.color="green"
  ```

