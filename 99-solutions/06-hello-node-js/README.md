# 06-hello-node-js


## Requirements

- Create a `go.mod` file
- Create a `main.go` file

## go.mod

```text
module 06-hello-nodejs

go 1.17
```

## main.go

```golang
package main

import (
	"encoding/json"
	"log"
	"syscall/js"
)

type Human struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

func GiveMeHumanJsonObject(this js.Value, args []js.Value) interface{} {

	jsonHuman, err := json.Marshal(
		Human{
			args[0].String(),
			args[1].String()})

	if err != nil {
		log.Fatalf(
			"Error occured during marshaling: %s",
			err.Error())
	}

	// Call native JavaScript feature
	JSON := js.Global().Get("JSON")
	jsonString := string(jsonHuman)

	// Call native JavaScript JSON method
	return JSON.Call("parse", jsonString)

}

func main() {

	js.Global().Set(
		"GiveMeHumanJsonObject",
		js.FuncOf(GiveMeHumanJsonObject))

	<-make(chan bool)
}

```

## Build, run, ...

```bash
# build the wasm fil
GOOS=js GOARCH=wasm go build -o main.wasm
```

```bash
# run the script
node index.js
```

